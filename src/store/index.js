import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cards: [
      {
        posX: 40,
        posY: 40,
      },
      {
        posX: 200,
        posY: 40,
      },
      {
        posX: 40,
        posY: 200,
      },
    ],
    selectId: null,
    selectSide: null,
    links: [],
  },
  getters: {
    CARDS(state) {
      return state.cards;
    },
    LINKS(state) {
      return state.links;
    },
    SELECT_ID(state) {
      return state.selectId;
    },
    SELECT_SIDE(state) {
      return state.selectSide;
    },
  },
  mutations: {
    SET_CARD_POSITION: (state, data) => {
      Vue.set(state.cards, data.id, {
        ...state.cards[data.id],
        posX: data.posX,
        posY: data.posY,
      });
    },
    UNSET_SELECT: (state) => {
      state.selectId = null;
      state.selectSide = null;
    },
    SET_SELECT: (state, data) => {
      state.selectId = data.id;
      state.selectSide = data.side;
    },
    ADD_LINE: (state, data) => {
      state.links.push({
        startId: state.selectId,
        endId: data.id,
        startSide: state.selectSide,
        endSide: data.side,
      });
    },
    CREATE_CARD: (state) => {
      state.cards.push({
        posX: 40,
        posY: 40,
      });
    },
  },
  actions: {
    CREATE_LINE({ state, commit }, data) {
      let deleteLine = null;
      state.links.forEach((item, index) => {
        if (
          (item.startId === state.selectId)
          && (item.endId === data.id)
          && (item.startSide === state.selectSide)
          && (item.endSide === data.side)
        ) {
          deleteLine = index;
        }
      });
      console.log(deleteLine);
      if (deleteLine !== null) {
        state.links.splice(deleteLine, 1);
        commit('UNSET_SELECT');
      } else {
        commit('ADD_LINE', data);
        commit('UNSET_SELECT');
      }
    },
  },
});
